import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  location: string;
  position: Position;
  geoEnabled: boolean;

  constructor() {
    this.location = '';
    this.position = <Position>{};
    this.geoEnabled = false;
  }

  ngOnInit() {
    this.checkGeoEnabled();
  }

  checkGeoEnabled(){
    if('geolocation' in window){
      this.geoEnabled = true;
    }
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => this.getCords(position),
      (error) => this.locationError(error));
  }


  getCords(position) {
    console.log(position);
    this.position = position;
    this.geoEnabled = true;
  }

  locationError(error) {
    console.log('error:' + error);
  }
}
