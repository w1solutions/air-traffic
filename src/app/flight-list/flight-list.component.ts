import {Component, Input, OnInit} from '@angular/core';
import {FlightsService} from './flights.service';
import {Flight} from '../shared/flight.model';



@Component({
  selector: 'app-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.css']
})
export class FlightListComponent implements OnInit {

  public response;
  public flights: Flight[] = [];
  @Input() latitude: string;
  @Input() longitude: string;

  constructor(private flightsService: FlightsService) {

  }

  ngOnInit() {
    this.getFlights(this.latitude, this.longitude);
  }

  getFlights(lat: string, lng: string) {
    this.flightsService.getFlights(lat, lng).subscribe(
      data => this.response = data,
      err => console.error(err),
      () => {
        console.log('done loading flights');
        for (let f of this.response['acList']) {
          this.flights.push(new Flight(f['Id'], f['Alt'], f['Man'], f['Mdl'], f['From'], f['To']));
        }
      }
    );
  }
}
