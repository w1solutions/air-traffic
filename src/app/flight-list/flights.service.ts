import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class FlightsService {

  constructor(private http: HttpClient) {}

  getFlights(lat, lng) {
    // https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?lat=44.798420799999995&lng=20.375951099999998&fDstL=0&fDstU=100
    return this.http.get('/api/VirtualRadar/AircraftList.json?lat=' + lat + '&lng=' + lng + '&fDstL=0&fDstU=100');
  }
}
