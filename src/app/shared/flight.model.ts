import {Injectable} from "@angular/core";

@Injectable()
export class Flight {

  id: string;
  altitude: string;
  manufacturer: string;
  model: string;
  origin: string;
  destination: string;

  constructor(id: string, alt: string, man: string, mdl: string, from: string, to: string) {
    this.id = id;
    this.altitude = alt;
    this.manufacturer = man;
    this.model = mdl;
    this.origin = from;
    this.destination = to;
  }
}
